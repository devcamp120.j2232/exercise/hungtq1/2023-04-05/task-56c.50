package com.devcamp.restapicustomeraccount.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.restapicustomeraccount.Controller.models.Account;

@Service
public class AccountService {
  @Autowired
  CustomerService customerService;

  ArrayList<Account> listAccounts = new ArrayList<Account>();

  public ArrayList<Account> getAccountList() {

    ArrayList<Account> accounts = new ArrayList<>();

    Account account1 = new Account(1, customerService.getCustomer1(), 200000);
    Account account2 = new Account(2, customerService.getCustomer2(), 180000);
    Account account3 = new Account(3, customerService.getCustomer3(), 170000);

    accounts.add(account1);
    accounts.add(account2);
    accounts.add(account3);
    return accounts;
  }

}
