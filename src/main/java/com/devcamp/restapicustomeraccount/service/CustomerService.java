package com.devcamp.restapicustomeraccount.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.restapicustomeraccount.Controller.models.Customer;

@Service
public class CustomerService {
  ArrayList<Customer> customers = new ArrayList<>();

  public ArrayList<Customer> getListCustomers() {
    ArrayList<Customer> ListCustomer = new ArrayList<>();
    ListCustomer.add(getCustomer1());
    ListCustomer.add(getCustomer2());
    ListCustomer.add(getCustomer3());
    return ListCustomer;
  }

  public Customer getCustomer1() {
    Customer customer1 = new Customer(1, "Teo", 30);
    return customer1;
  }

  public Customer getCustomer2() {
    Customer customer2 = new Customer(2, "Bich", 25);
    return customer2;
  }

  public Customer getCustomer3() {
    Customer customer3 = new Customer(3, "Chinh", 20);
    return customer3;
  }
}
