package com.devcamp.restapicustomeraccount.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapicustomeraccount.service.AccountService;
import com.devcamp.restapicustomeraccount.Controller.models.Account;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AccountController {
  @Autowired
  AccountService accountService;

  @GetMapping("/accounts")
  public ArrayList<Account> getAccountList() {
    return accountService.getAccountList();
  }
}
